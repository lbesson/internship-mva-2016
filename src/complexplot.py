#! /usr/bin/env python3
# -*- coding: utf-8; mode: python -*-
""" Small toolbox to plot a complex-valued 2D image to a RBG-valued 2D array.

About:

- *Date:* Friday 24 June 2016.
- *Author:* Lilian Besson, for the MVA Master, (C) 2015-16.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

from __future__ import print_function, division, absolute_import  # Python 2/3 compatibility

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import hsv_to_rgb


def noticks():
    """ Remove ticks from the current plot."""
    plt.xticks([])
    plt.yticks([])


def Complex2HSV(z, saturation=1.0, hue_start=+180, maxamp=0.80, truncate=False):
    """ Convert a complex-valued 2D array z (with amplitude from rmin to rmax) to a RBG-valued 2D array (shape (3, size, size)) that can be plotted with plt.imshow.

    - From http://stackoverflow.com/a/36082859/5889533
    - More on HSV here: https://en.wikipedia.org/wiki/HSL_and_HSV
    """
    if z.dtype != complex:
        return z
    # get amplidude of z and limit to [rmin, rmax]
    amp = np.abs(z)
    rmin = np.min(amp)
    rmax = np.max(amp)
    if truncate:
        rmax = maxamp * np.max(amp)
        amp = np.where(amp < rmin, rmin, amp)
        amp = np.where(amp > rmax, rmax, amp)
    phi = np.angle(z, deg=True) + hue_start  # Angles in degrees
    # HSV are values in range [0, 1]
    h = (phi % 360) / 360  # Hue is the angle
    s = saturation * np.ones_like(h)  # Saturation at 0.85, constant
    v = (amp - rmin) / (rmax - rmin)  # Value is the amplitude, linearly scaled in [0, 1]
    return hsv_to_rgb(np.dstack((h, s, v)))


def visualizeHSVColorMap(N=2**10, save=False):
    """ Plot a colored complex-valued 2D image to visualize the effect of Complex2HSV. """
    x, y = np.ogrid[-1:1:(N * 1j), -1:1:(N * 1j)]  # (N * 1j) to include the end point
    z = y + 1j * x
    img = Complex2HSV(z)
    plt.figure()
    plt.hold(True)
    plt.plot([N / 2], [N / 2], 'w+', ms=10)
    plt.imshow(img)
    plt.title(r"Complex-valued $z = \rho \mathrm{e}^{i\phi}$ 2D image in HSV : Hue = $\phi$, Value = $\rho$.")
    noticks()
    cbar = plt.colorbar()
    cbar.set_ticks([0, 1 / 3, 2 / 3, 1])
    cbar.set_label(r'Phase $\phi$')
    cbar.set_ticklabels([r'$-\pi/2$', r'$0$', r'$\pi/2$', r'$\pi$'])
    if save:
        plt.savefig("complexplot.png", dpi=160)  # Comment after saving the figure
    plt.show()


def fullWindow(verb=True):
    """ Display the current matplotlib plot in full window.

    - XXX Experimental https://stackoverflow.com/q/12439588/
    """
    if verb:
        print("    Trying to maximize a matplotlib window with fullWindow(): backend =", plt.get_backend())
    figManager = plt.get_current_fig_manager()
    try:
        figManager.frame.Maximize(True)
    except Exception as e:
        if verb:
            print("    - Trying 'figManager.frame.Maximize(True)'... It failed (exception = {}) !".format(e))
    try:
        figManager.window.state('zoomed')
    except Exception as e:
        if verb:
            print("    - Trying 'figManager.window.state('zoomed')... It failed (exception = {}) !".format(e))
    try:
        figManager.window.showMaximized()
    except Exception as e:
        if verb:
            print("    - Trying 'figManager.window.showMaximized()' ... It failed (exception = {}) !".format(e))


__all__ = [Complex2HSV, visualizeHSVColorMap, fullWindow, noticks]

# End of complexplot.py
