#! /usr/bin/env python3
# -*- coding: utf-8; mode: python -*-
""" Test of one or more Fractional-Directional Hilbert Transform (fdHT) applied to test images in 2D.

About:

- This script is a companion of my internship report, see https://bitbucket.org/lbesson/internship-mva-2016.
- fdHT and details on the implementation are given in the part A.3.5 of my report, cf. https://goo.gl/xPzw4A


Remarks:

- Only square n-dim images are used (of shape (size,)*n = (size, size, .., size)).
- The dimension n is left free in the script, but we only intend to use it with n = 2.
- See this page for some details about fftn and ifftn: https://docs.scipy.org/doc/numpy/reference/routines.fft.html#higher-dimensions


About:

- *Date:* Friday 24 June 2016.
- *Author:* Lilian Besson, for the MVA Master, (C) 2015-16.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

from __future__ import print_function, division, absolute_import  # Python 2/3 compatibility

from time import sleep

import numpy as np
from numpy import fft

try:
    from matplotlib import use
    use('Qt4Agg')
except Exception as e:
    print("WARNING: Impossible to set the matplotlib backend to Qt4Agg (the animation will probably not work)...")
    print("Exception:", e)
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

from scipy import misc  # For some examples of pictures
# Cf. http://www.scipy-lectures.org/advanced/image_processing/#opening-and-writing-to-image-files

from complexplot import Complex2HSV, visualizeHSVColorMap, fullWindow, noticks


# Change the amplitude of the Gaussian/Laplace blob (they have variance 1 on a [-AMPLITUDE, AMPLITUDE] grid)
# AMPLITUDE = 200  # Just a Dirac at (0, 0)
# AMPLITUDE = 50   # A very small Gaussian blob at (0, 0)
# AMPLITUDE = 10   # A small Gaussian blob at (0, 0)
# AMPLITUDE = 4    # A Gaussian blob at (0, 0)
AMPLITUDE = 2    # A large Gaussian blob at (0, 0)


# %% Fourier multiplier of fdHT

def one_fdHT(omega, tau, u):
    r""" Compute the Fourier response of ONE n-dimensional Fractional-Directional Hilbert Transform: ::

    ^H_tau,u (omega) = exp(j * pi * tau * sign(< omega , u >)).


    .. math:: \widehat{\mathcal{H}_{\tau, \overrightarrow{u}}}(\omega) = \exp(j \pi \tau \mathrm{sign}(\langle \omega , \overrightarrow{u} \rangle)).

    - It is applied to the frequency array omega, with parameters tau and u.
    - omega has shape (size, ..., size, n) and u has shape (n,) : the dot product apply to the last coordinate.

    Special cases:

    - Use tau = 1/2 and u = [cos(theta), sin(theta)] to get the natural n-dimensional Directional Hilbert Transform.
    - Use tau = 1 [mod 2] to get - Identity.
    - Use tau = 0 [mod 2] to get + Identity.
    """
    angular_profile = tau * np.sign(np.dot(omega, u))
    return np.exp(1j * np.pi * angular_profile)


def multi_fdHT(omega, tau, u):
    r""" Compute the Fourier response of a composition of K n-dimensional Fractional-Directional Hilbert Transform: ::

    ^H_tauk,uk (omega) = exp(j * pi * (tau_1 * sign(< omega , u_1 >) + ... + tau_K * sign(< omega , u_K >)).


    .. math:: \widehat{\mathcal{H}_{\tau_{1:K}, \overrightarrow{u}_{1:K}}}(\omega) = \exp\bigl(j \pi \sum\limits_{k=1}^{K} \tau_k \mathrm{sign}(\langle \omega , \overrightarrow{u_k} \rangle)\bigr).

    - It is applied to the frequency array omega, with parameters tau and u.
    - omega has shape (size, ..., size, n) and u has shape (n,) : the dot product apply to the last coordinate.
    """
    angular_profile = 0
    for tau_i, u_i in zip(tau, u):
        angular_profile += tau_i * np.sign(np.dot(omega, u_i))
    return np.exp(1j * np.pi * angular_profile)


# %% Apply fdHT to an image, on the Fourier domain

def apply_fdHT(image, tau=0, theta=None, u=None, n=None, size=None,
               returnFourier=False,
               use_rfft=False  # FIXME Experimental!
               ):
    r""" Apply one or more n-dimensional Fractional-Directional Hilbert Transform to the image, on the Fourier domain (and coming back with ifftn).

    .. math:: \mathcal{H}_{\tau, \overrightarrow{u}} \{f\} (x) \leftrightarrow_{\mathcal{F}} \widehat{\mathcal{H}_{\tau, \overrightarrow{u}} f}(\omega) = \exp(j \pi \tau \mathrm{sign}(\langle \overrightarrow{u}, \omega \rangle)) \widehat{f}(\omega).

    - Default values for tau is 0 (in which case u or theta does not matter: H = Id).
    - If n and size are absent, we use the dimension and size of the image.

    - If theta is given, :math:`u = u_{\theta} := [ \cos(\theta), \sin(\theta) ]` is used instead of u (in the n = 2 setting).

    - If returnFourier, the Fourier transformed image is returned instead of the completely transformed image.

    - XXX If use_rfft, Experimentally using rfftn because the input is REAL (cf. https://docs.scipy.org/doc/numpy/reference/routines.fft.html#real-ffts).
    """
    print("Starting apply_fdHT(image, tau, theta, u, n, size) ...")
    # Handling default values :
    if theta is not None:
        if n is not None:
            assert n == 2, "Error in apply_fdHT(...) : theta can be given only if n = 2"
        # K (non-zero) directional vector in R^2, of angular parameter theta.
        u = np.transpose(np.array([np.cos(theta), np.sin(theta)]))
        # Note: u.T is important to distribute the different theta_i correctly: u has shape (K, 2) not (2, K).
    if n is None:  # Guess n
        n = len(np.shape(image))
    if size is None:  # Guess size
        size = np.shape(image)[0]
        assert all(dim == size for dim in np.shape(image)), "Error in apply_fdHT(...) : non-square image!"
        # TODO? Improve support for non-square image!
    if u is None:  # Guess u
        u = np.zeros(n)
    print("1. n =", n, "and size =", size)
    print("2. Shape of image :", np.shape(image))
    print("3. Shape of tau :", np.shape(tau), ": tau = ", tau)
    is_simple_fdHT = (len(np.shape(tau)) == 0)
    print("4. Shape of theta :", np.shape(theta), ": theta = ", theta)
    print("5. Shape of u :", np.shape(u), ": u = ", u)

    if use_rfft:  # XXX Experimentally using rfftn because the input is REAL
        print("use_rfft = True: Experimentally using fft.rfft2(image) because the input is REAL ...")
        fourier_image = fft.rfft2(image)  # XXX Use rfftn instead!
    else:
        fourier_image = fft.fftn(image)
    print("6. Shape of fourier_image :", np.shape(fourier_image))
    if use_rfft:  # XXX Experimentally using rfftn because the input is REAL
        print("use_rfft = True: Experimentally using fft.rfftfreq(size) because the input is REAL ...")
        freqs = fft.rfftfreq(size)
    else:
        freqs = fft.fftfreq(size)
    freqs = np.sort(freqs, axis=None)  # FIXED Apparently, we should order the frequency values
    # XXX We could use fft.fftshift instead, but I don't see the point...
    # https://docs.scipy.org/doc/numpy/reference/generated/numpy.fft.fftshift.html#numpy.fft.fftshift
    print("7. Shape of freqs :", np.shape(freqs))
    # It seems OK, cf. https://docs.scipy.org/doc/numpy/reference/routines.fft.html#implementation-details
    # In 2D : https://docs.scipy.org/doc/numpy/reference/routines.fft.html#higher-dimensions
    omega_grid = np.array(np.meshgrid(*(freqs,) * n))
    omega = omega_grid.T
    # array of shape (size, ..., size, n) = (*([size]*n + [n]))
    # We reshape omega because it used in a np.dot(omega, u) : n values on the last axis (u has n values)
    print("8. Shape of omega :", np.shape(omega))
    # Should we apply one or more fdHT ?
    if is_simple_fdHT:
        print("9. Using one_fdHT with tau = {} and u = {}.".format(tau, u))
        fourier_multiplier = one_fdHT(omega, tau, u)
    else:
        K = np.size(tau)  # tau is a an array, u should also be a multi-array
        print("9. Using multi_fdHT with K = {} composed fdHT, with tau 1..K = {} and u 1..K = {}.".format(K, tau, u))
        fourier_multiplier = multi_fdHT(omega, tau, u)
    if use_rfft:  # XXX Experimentally using rfftn because the input is REAL
        fourier_multiplier = np.vstack((fourier_multiplier[:-1, :].conj(), fourier_multiplier[1:, :]))
    print("10. Shape of fourier_multiplier :", np.shape(fourier_multiplier))
    if returnFourier:
        return fourier_image * fourier_multiplier  # Point-wise mult
    if use_rfft:  # XXX Experimentally using rfftn because the input is REAL
        print("use_rfft = True: Experimentally using fft.irfft2(fourier_image * fourier_multiplier) because the input is REAL ...")
        transformed_image = fft.irfft2(fourier_image * fourier_multiplier)  # Transform it really !  # XXX Use rfftn instead!
    else:
        transformed_image = fft.ifftn(fourier_image * fourier_multiplier)  # Transform it really !
    print("11. Shape of transformed_image :", np.shape(transformed_image))
    print("    And dtype of transformed_image :", transformed_image.dtype)
    print("- Average, std, min, max of imag part of transformed_image:")
    print("    - transformed_image.imag.mean() =", transformed_image.imag.mean())
    print("    - transformed_image.imag.std() =", transformed_image.imag.std())
    print("    - transformed_image.imag.min() =", transformed_image.imag.min())
    print("    - transformed_image.imag.max() =", transformed_image.imag.max())
    return transformed_image


# Generate images

def white(X):
    """ Dummy generator for our 'interest' image: a simple white (= 0) background of correct size.
    """
    return np.zeros(np.shape(X)[1:])


def black(X):
    """ Dummy generator for our 'interest' image: a simple black (= 1) background of correct size.
    """
    return np.ones(np.shape(X)[1:])


def centered_gaussian(X):
    """ Default generator for our 'interest' image: a simple n-dim Gaussian image, centered in (0, 0) and localized on [-4, 4], of variance 1.

    - X is an array of shape (n, size, ..., size) where X[:, i1, ..., in] is the n-dimensional (vectorial) value at the location (i1, ..., in) in the meshgrid.
    - In practice, n = 2 and so X[:, i, j] has 2 components: x[i, j] and y[i, j] (or a complex component).
    """
    image = np.exp(- np.sum(X ** 2, axis=0) / 2)
    return image / np.max(image)  # Scaled in [0, 1]


def centered_laplace(X):
    """ Generator for our 'interest' image: a simple n-dim Laplace image, centered in (0, 0) and localized on [-4, 4], of variance 1.

    - X is an array of shape (n, size, ..., size) where X[:, i1, ..., in] is the n-dimensional (vectorial) value at the location (i1, ..., in) in the meshgrid.
    - In practice, n = 2 and so X[:, i, j] has 2 components: x[i, j] and y[i, j] (or a complex component).
    """
    image = np.exp(- np.sum(np.abs(X), axis=0) / 2)
    return image / np.max(image)  # Scaled in [0, 1]


def rotated_half(X, theta=np.pi / 8, smooth=False):
    """ Generator for our 'interest' image: a image, white on one half and black on the other half, separated by a plane of angle theta.

    - X is an array of shape (n, size, ..., size) where X[:, i1, ..., in] is the n-dimensional (vectorial) value at the location (i1, ..., in) in the meshgrid.
    - In practice, n = 2 and so X[:, i, j] has 2 components: x[i, j] and y[i, j] (or a complex component).
    """
    assert len(np.shape(X)) == 3, "Error in rotated_half(X, theta) : X should have shape (n, size, size), but has shape {} ...".format(np.shape(X))
    X /= 0.5 * np.max(X)  # Center in [-1, 1]
    n = np.shape(X)[0]
    assert n == 2, "Error in rotated_half(X, theta) : n should be = 2, but is {} ...".format(n)
    angles = np.angle(X[0] + 1j * X[1])
    signs = - np.sign(np.sin(angles + theta))
    if not smooth:
        return signs
    else:
        # We smooth it by multiplying with a Gaussian
        image = signs * np.exp(- np.sum(X ** 2, axis=0) / 2)
        return image / np.max(image)  # Scaled in [0, 1]
    # XXX I should have used scipy.ndimage.rotate(image, angle) instead


# TODO Comment/uncomment here to change the default one
# default_generator = white  # Useless!
# default_generator = black  # Useless!
default_generator = centered_gaussian
# default_generator = centered_laplace
# default_generator = rotated_half
# default_generator = lambda X: rotated_half(X, smooth=True)


def generate_example(n=2, size=2**10, f=default_generator, amplitude=AMPLITUDE, origin=0):
    """ Create an 'interest' square n-dimensional image, by applying f to a grid of shape (size,)*n, on [-amplitude, amplitude] in each dimension (centered in origin, defaut is 0).
    """
    x = origin + np.linspace(-amplitude, amplitude, size)  # 128 points regularly spaced on [-4, 4] (if size = 128)
    # array of shape (n, size, ..., size) = (n, *(size,) * n)
    X = np.array(np.meshgrid(*(x,) * n)).reshape(n, *(size,) * n)
    if f is None:
        return np.zeros((size,) * n)
    else:
        return f(X)


# %% Test our apply_fdHT

def apply_and_plot(tau=0, theta=None, offset=None,
                   save=True, lena=False,
                   plot_fdht=True,
                   plot_fourier=False, plot_fdht_fourier=False,
                   plot_fourier_color=False, plot_color=False):
    """ Test the apply_fdHT(tau, theta) on a generated image, display both the original and transformed image, and save it.
    """
    # Get an image
    if lena:
        image = misc.lena() / 255.0
    else:
        # origin = AMPLITUDE / 2  # XXX To shift the center of the image
        origin = 0  # Do not shift the center of the image
        image = generate_example(origin=origin)
    assert len(np.shape(image)) == 2, "Error in apply_and_plot(...): image (= {}) should be square, but it has shape {} ...".format("'misc.lena() / 255.0'" if lena else "'generate_example()'", np.shape(image))
    sizex, sizey = np.shape(image)  # Will fail if image is not squared
    assert sizex == sizey, "Error in apply_and_plot(...): only square images are supported right now (size = ({} x {}))...".format(sizex, sizey)
    size = sizex

    # Detect what should be done and plotted
    is_simple_fdHT = (len(np.shape(tau)) == 0)
    K = np.size(tau)
    print("==> apply_and_plot : K =", K)
    # Transform it
    transformed_image = apply_fdHT(image, tau=tau, theta=theta)
    print("FIXME It should be real, as H_{tau, u} is an operator on L^2(R^n)...")
    print("Indeed, its Fourier multiplier is Hermitian symmetric (H(-w) = H(w)*)")
    # See https://en.wikipedia.org/wiki/Peak_signal-to-noise_ratio
    # And see https://stackoverflow.com/questions/9556442/how-to-measure-peak-signal-to-noise-ratio-of-images
    print("\n==> We can evaluate the 'error' : PSNR between Re(H image) and image")
    max_intensity = 1.0
    PSNR = 20 * np.log10(max_intensity) - 10 * np.log10(np.mean(np.imag(transformed_image) ** 2))
    print("\nI evaluated the PSNR (pick signal-to-noise ratio) between the original image and the Real part of the Fractional-Directional Hilbert Transform of the image to be = {:.4g} dB ...".format(PSNR))

    # Then plot the first figure
    if plot_fourier:
        print("+ Plotting the first figure ... (plot_fourier = True)")
        plt.figure()
        plt.hold(True)
        # Original image
        plt.subplot(2, 2, 1)  # 1st [ +  ., . . ]
        plt.imshow(image, interpolation='none', cmap='gray')
        plt.title("Original image ({} x {})".format(size, size))
        plt.colorbar()  # XXX should be the same colorbar for each plot!
        fourier_image = fft.fftn(image)
        # Fourier image
        # 2nd [ .  +, . . ]
        plt.subplot(2, 2, 2)
        plt.imshow(np.real(fourier_image), interpolation='none', cmap='gray')
        plt.title("Fourier response (real part)")
        plt.colorbar()  # XXX should be the same colorbar for each plot!
        # 3rd [ .  ., + . ]
        plt.subplot(2, 2, 3)
        plt.imshow(np.abs(fourier_image), interpolation='none', cmap='gray')
        plt.title("Fourier response (modulus)")
        plt.colorbar()  # XXX should be the same colorbar for each plot!
        # 4th [ . ., . + ]
        plt.subplot(2, 2, 4)
        plt.imshow(np.imag(fourier_image), interpolation='none', cmap='gray')
        plt.title("Fourier response (imaginary part)")
        plt.colorbar()  # XXX should be the same colorbar for each plot!
        plt.suptitle("Original image and its Fourier response", fontsize=18)
        plt.show()
        fullWindow()

    # Then plot the 2nd figure
    if plot_fdht_fourier:
        print("+ Plotting the 2nd figure ... (plot_fdht_fourier = True)")
        plt.figure()
        plt.hold(True)
        # Original image
        fourier_image = fft.fftn(image)
        # 1st [ +  ., . . ]
        plt.subplot(2, 2, 1)
        plt.imshow(np.real(fourier_image), interpolation='none', cmap='gray')
        plt.title("Original image (real part) in Fourier, mean = {:.2g}".format(np.mean(np.real(fourier_image))))
        plt.colorbar()  # XXX should be the same colorbar for each plot!
        # 2nd [ .  +, . . ]
        plt.subplot(2, 2, 2)
        plt.imshow(np.imag(fourier_image), interpolation='none', cmap='gray')
        plt.title("Original image (imaginary part) in Fourier, mean = {:.2g}".format(np.mean(np.imag(fourier_image))))
        plt.colorbar()  # XXX should be the same colorbar for each plot!
        # Transformed image
        fourier_transformed_image = apply_fdHT(image, tau=tau, theta=theta, returnFourier=True)
        # 3rd [ .  ., + . ]
        plt.subplot(2, 2, 3)
        plt.imshow(np.real(fourier_transformed_image), interpolation='none', cmap='gray')
        plt.title("Transformed image (real part) in Fourier, mean = {:.2g}".format(np.mean(np.real(fourier_transformed_image))))
        plt.colorbar()  # XXX should be the same colorbar for each plot!
        # 4th [ . ., . + ]
        plt.subplot(2, 2, 4)
        plt.imshow(np.imag(fourier_transformed_image), interpolation='none', cmap='gray')
        plt.title("Transformed image (imaginary part) in Fourier, mean = {:.2g}".format(np.mean(np.imag(fourier_transformed_image))))
        plt.colorbar()  # XXX should be the same colorbar for each plot!
        # Put a nice overall title!
        if is_simple_fdHT:
            str_tau = str(np.round(tau, 3))
            str_theta = str(np.round(theta, 3))
        else:
            str_tau = str([round(taui, 3) for taui in tau])
            str_theta = str([round(thetai, 3) for thetai in theta])
        str_latex_fdHT = r"$\mathcal{H}_{\tau, \overrightarrow{u}_{\theta}}}(I)$"
        plt.suptitle(r"{}fdHT {} in Fourier with $\tau = {}$ and $\theta = {}$, PSNR($I$, $Re$(fdHT($I$)) = ${:.4g}$".format("" if is_simple_fdHT else "Multi-", str_latex_fdHT, str_tau, str_theta, PSNR), fontsize=16)
        # Save if we asked to
        if save:
            if offset is None:
                outname = "fdHT__fourier.png"
            elif is_simple_fdHT:
                outname = "fdHT__fourier__fig{}.png".format(offset)
            else:
                outname = "multi_fdHT__fourier__K={}__fig{}.png".format(K, offset)
            print("Saving to '{}' ...".format(outname))
            plt.savefig(outname, dpi=180)
        # Show
        plt.show()
        fullWindow()

    # Then plot the 3rd figure
    if plot_fdht:
        print("+ Plotting the 3rd figure ... (plot_fdht = True)")
        plt.figure()
        plt.hold(True)
        # Original image
        plt.subplot(2, 2, 1)  # 1st [ +  ., . . ]
        plt.imshow(image, interpolation='none', cmap='gray')
        plt.title("Original image ({} x {})".format(size, size))
        plt.colorbar()  # XXX should be the same colorbar for each plot!
        # Transformed image
        # 2nd [ .  +, . . ]
        plt.subplot(2, 2, 2)
        plt.imshow(np.real(transformed_image), interpolation='none', cmap='gray')
        plt.title("Transformed image (real part)")
        plt.colorbar()  # XXX should be the same colorbar for each plot!
        # 3rd [ .  ., + . ]
        plt.subplot(2, 2, 3)
        plt.imshow(np.abs(transformed_image), interpolation='none', cmap='gray')
        plt.title("Transformed image (modulus)")
        plt.colorbar()  # XXX should be the same colorbar for each plot!
        # 4th [ . ., . + ]
        plt.subplot(2, 2, 4)
        plt.imshow(np.imag(transformed_image), interpolation='none', cmap='gray')
        plt.title("Transformed image (imaginary part)")
        plt.colorbar()  # XXX should be the same colorbar for each plot!
        # Put a nice overall title!
        if is_simple_fdHT:
            str_tau = str(np.round(tau, 3))
            str_theta = str(np.round(theta, 3))
        else:
            str_tau = str([round(taui, 3) for taui in tau])
            str_theta = str([round(thetai, 3) for thetai in theta])
        str_latex_fdHT = r"$\mathcal{H}_{\tau, \overrightarrow{u}_{\theta}}}(I)$"
        plt.suptitle(r"{}fdHT {} with $\tau = {}$ and $\theta = {}$, PSNR($I$, $Re$(fdHT($I$)) = ${:.4g}$".format("" if is_simple_fdHT else "Multi-", str_latex_fdHT, str_tau, str_theta, PSNR), fontsize=16)
        # Save if we asked to
        if save:
            if offset is None:
                outname = "fdHT.png"
            elif is_simple_fdHT:
                outname = "fdHT__fig{}.png".format(offset)
            else:
                outname = "multi_fdHT__K={}__fig{}.png".format(K, offset)
            print("Saving to '{}' ...".format(outname))
            plt.savefig(outname, dpi=180)
        # Show
        plt.show()
        fullWindow()

    # Then plot the 4th figure
    if plot_fourier_color:
        print("+ Plotting the 4th figure ... (plot_fourier_color = True)")
        plt.figure()
        plt.hold(True)
        # 1st [ +  ., . . ]
        plt.subplot(2, 2, 1)
        plt.imshow(image, interpolation='none', cmap='gray')  # Original image
        plt.title("Original image ({} x {})".format(size, size))
        plt.colorbar()  # XXX should be the same colorbar for each plot!
        # 2nd [ .  +, . . ]
        plt.subplot(2, 2, 2)
        fourier_image = fft.fftn(image)
        fourier_image_rgb = Complex2HSV(fourier_image)
        plt.imshow(fourier_image_rgb, interpolation='none')
        plt.title("Original image in Fourier (HSV colored)")
        plt.colorbar()  # XXX should be the same colorbar for each plot!
        # 3rd [ .  ., + . ]
        transformed_image_rgb = Complex2HSV(transformed_image)
        plt.subplot(2, 2, 3)
        plt.imshow(transformed_image_rgb, interpolation='none')  # Transformed image
        plt.title("Transformed image (complex-valued, should not)")
        plt.colorbar()  # XXX should be the same colorbar for each plot!
        # 4th [ . ., . + ]
        fourier_transformed_image = apply_fdHT(image, tau=tau, theta=theta, returnFourier=True)
        fourier_transformed_image_rgb = Complex2HSV(fourier_transformed_image)
        plt.subplot(2, 2, 4)
        plt.imshow(fourier_transformed_image_rgb, interpolation='none')
        plt.title("Transformed image in Fourier (HSV colored)")
        plt.colorbar()  # XXX should be the same colorbar for each plot!
        # Put a nice overall title!
        if is_simple_fdHT:
            str_tau = str(np.round(tau, 3))
            str_theta = str(np.round(theta, 3))
        else:
            str_tau = str([round(taui, 3) for taui in tau])
            str_theta = str([round(thetai, 3) for thetai in theta])
        str_latex_fdHT = r"$\mathcal{H}_{\tau, \overrightarrow{u}_{\theta}}}(I)$"
        plt.suptitle(r"Colored {}fdHT {} in Fourier with $\tau = {}$ and $\theta = {}$, PSNR($I$, $Re$(fdHT($I$)) = ${:.4g}$".format("" if is_simple_fdHT else "Multi-", str_latex_fdHT, str_tau, str_theta, PSNR), fontsize=16)
        # Show
        plt.show()
        fullWindow()

    # Then plot the 5th figure
    if plot_color:
        print("+ Plotting the 5th figure ... (plot_color = True)")
        plt.figure()
        plt.hold(True)
        # Original image
        # 1st [ +  ., . . ]
        plt.subplot(1, 2, 1)
        plt.imshow(image, interpolation='none', cmap='gray')
        plt.title("Original image ({} x {})".format(size, size))
        plt.colorbar()  # XXX should be the same colorbar for each plot!
        transformed_image_rgb = Complex2HSV(transformed_image)
        plt.subplot(1, 2, 2)
        plt.imshow(transformed_image_rgb, interpolation='none')
        plt.title("Transformed image (complex-valued, should not)")
        plt.colorbar()  # XXX should be the same colorbar for each plot!
        # Put a nice overall title!
        if is_simple_fdHT:
            str_tau = str(np.round(tau, 3))
            str_theta = str(np.round(theta, 3))
        else:
            str_tau = str([round(taui, 3) for taui in tau])
            str_theta = str([round(thetai, 3) for thetai in theta])
        str_latex_fdHT = r"$\mathcal{H}_{\tau, \overrightarrow{u}_{\theta}}}(I)$"
        plt.suptitle(r"Colored {}fdHT {} with $\tau = {}$ and $\theta = {}$, PSNR($I$, $Re$(fdHT($I$)) = ${:.4g}$".format("" if is_simple_fdHT else "Multi-", str_latex_fdHT, str_tau, str_theta, PSNR), fontsize=16)
        # Show
        plt.show()
        fullWindow()

    return transformed_image, PSNR


def apply_and_turn(offset=None, lena=False,
                   save=True, turn_input=False, move_tau=False,
                   theta0=-np.pi / 2,  # TODO Change here the default values for the parameter theta
                   tau0=-0.5  # TODO Change here the default values for the parameter tau
                  ):
    """ Test the apply_fdHT(tau, theta) on a generated image, display both the original and transformed image, for:

    - theta = [0, 2 pi] slowly turning if not turn_input nor move_tau, with tau = tau0,
    - tau = [-2, 2] slowly growing if move_tau, with theta = theta0,
    - for a turning input (half-plane) if turn_input, with theta = theta0 and tau = tau0.
    """
    origin = 0  # Do not shift the center of the image
    # Get an image
    if lena:
        image = misc.lena() / 255.0
    else:
        # origin = AMPLITUDE / 2  # TODO To shift the center of the image, if you need
        image = generate_example(origin=origin)
    assert len(np.shape(image)) == 2, "Error in apply_and_plot(...): image (= {}) should be square, but it has shape {} ...".format("'misc.lena() / 255.0'" if lena else "'generate_example()'", np.shape(image))
    sizex, sizey = np.shape(image)  # Will fail if image is not squared
    assert sizex == sizey, "Error in apply_and_plot(...): only square images are supported right now (size = ({} x {}))...".format(sizex, sizey)
    size = sizex

    # Then plot the figure
    print("+ Plotting the figure (no animation yet) ...")
    fig = plt.figure()
    plt.hold(True)
    # Original image
    plt.subplot(1, 2, 1)
    imshow1_manager = plt.imshow(image, interpolation='none', cmap='gray', animated=True)
    noticks()
    plt.title(r"Real input (${} \times {}$ px)".format(size, size))
    # plt.colorbar()  # XXX should be the same colorbar for each plot!
    transformed_image = apply_fdHT(image, tau=tau0, theta=theta0)
    # Transform it
    transformed_image_rgb = Complex2HSV(transformed_image)
    plt.subplot(1, 2, 2)
    imshow2_manager = plt.imshow(transformed_image_rgb, interpolation='none', animated=True)
    noticks()
    plt.title("Transformed image (complex)")
    # Put a nice overall title!
    str_tau = str(np.round(tau0, 3))
    str_theta = str(np.round(theta0, 3))
    str_latex_fdHT = r"$\mathcal{H}_{\tau, \overrightarrow{u}_{\theta}}}(I)$"
    suptitle_manager = plt.suptitle(r"Colored fdHT {} with $\tau = {}$ and $\theta = {}$ (turning)".format(str_latex_fdHT, str_tau, str_theta), fontsize=16)
    # plt.colorbar()  # XXX should be the same colorbar for each plot!
    # plt.show()  # XXX Should we plt.show() before the animation?
    fullWindow()  # XXX It seems to work sometimes but maybe not mandatory

    # Configure the animation
    N = 51
    all_theta = np.linspace(-np.pi, np.pi, N)
    all_tau = np.linspace(-2, 2, N)

    # Change here if you want the input to turn or the fdHT to turn
    move_tau = move_tau and (not turn_input)
    turn_input = turn_input and (not lena) and (not move_tau)

    def changeimage(i):
        """ Tries to update the images and suptitle of the plot."""
        print("--> apply_and_turn(): calling changeimage, i =", i, "...")
        if i >= N:
            return  # Does nothing
        if move_tau:
            tau = all_tau[i % N]
            theta = theta0
        else:
            tau = tau0
            theta = all_theta[i % N]
        print("    apply_and_turn(): theta =", theta, "and tau =", tau, "now updating the animation...")
        # 1. Update the image
        # 1.a. We can turn the image (if it is generated with rotated_half)
        if turn_input:
            # f = lambda X: rotated_half(X, theta=theta, smooth=smooth)  # Smooth it!
            f = lambda X: rotated_half(X, theta=theta)  # Dont' smooth it!
            local_image = generate_example(origin=origin, f=f)
            imshow1_manager.set_data(local_image)
            theta = theta0  # For the fdHT use the default theta0
        else:
            local_image = image
        # 1.b. Or we can apply different directional Hilbert Transform
        transformed_image = apply_fdHT(local_image, tau=tau, theta=theta)
        imshow2_manager.set_data(Complex2HSV(transformed_image))
        # 2. Update the sup-title
        if not turn_input:
            str_tau = str(np.round(tau, 3))
            str_theta = str(np.round(theta, 3))
            suptitle_manager.set_text(r"Colored fdHT {} with $\tau = {}$ {} and $\theta = {}$ {}".format(
                str_latex_fdHT,
                str_tau, "(moving)" if move_tau else "",
                str_theta, "(turning)" if not (turn_input or move_tau) else ""))
        # plt.show()  # XXX Should we plt.show() inside the animation?
        print("    apply_and_turn(): done in changeimage, i =", i, "theta =", theta, "tau =", tau, "...")
        # Should we return the object, XXX only if blit=True
        return imshow1_manager, imshow2_manager, suptitle_manager

    if save:  # FIXED Saving the animation now works fine (even if the animation is not displayed while being saved)
        print("> Starting to save the animation :")
        plt.show()  # XXX Should we plt.show() before the animation?
        sleep(20)  # It seems to be a good idea to wait a little bit
        animtosave = FuncAnimation(fig, changeimage, interval=100, frames=N, repeat=False)
        plt.show()
        outname = 'fdHT__colored_{}__{}{}{}{}{}{}'.format(
            offset if offset is not None else 1,
            "__lena" if lena else "",
            "__turn_input" if turn_input else "",
            "__move_tau" if move_tau else "",
            "__tau=" + str(np.round(tau0, 3)) if not move_tau else "",
            "__turn_theta" if not move_tau else "",
            "__theta=" + str(np.round(theta0, 3)) if not move_tau else ""
        )
        print("> Saving to '", outname + '.gif', "' with writer = 'imagemagick' and dpi = 180 ...")
        animtosave.save(outname + '.gif', writer='imagemagick', dpi=150, metadata={'copyright': "Lilian Besson, (C) 2016"})  # , fps=6
        print("> Done saving the animation !")
        # animtosave.save(outname + '.mp4', writer='ffmpeg', dpi=180)  # , fps=6
        print("Animation is done, it was saved to {}.gif".format(outname))
        return outname
    else:
        # Start the animation
        plt.show()  # XXX Should we plt.show() before the animation?
        sleep(2)  # It seems to be a good idea to wait a little bit
        print("> Starting the animation...")
        print("Calling 'anim = FuncAnimation(fig, changeimage, interval=100, frames=N, repeat=False)'")
        anim = FuncAnimation(fig, changeimage, interval=100, frames=N, repeat=False)
        # http://matplotlib.org/api/animation_api.html#matplotlib.animation.FuncAnimation
        print("> Done for the animation !")
        plt.show()
    # Done
    if False:  # FIXED Try to manually launch the animation? Not necessary
        sleep(1)  # It seems to be a good idea to wait a little bit
        print("> Starting the animation manually ! i = 0 ...", N)
        for i in range(N):
            changeimage(i)
            plt.show()
            sleep(0.5)
        # plt.show()
        print("> Done for the animation manually !")
    return origin, turn_input, move_tau, tau0, theta0, all_theta, all_tau, image, imshow1_manager, imshow2_manager, suptitle_manager


def main(lena=False, animation=False, multi=False, save=False, turn_input=False, move_tau=False, **kwargs):
    """ Main function, launches the experiments.
    """
    offset = 1
    # TODO Change here to play with the amplitude of the tau_i
    # amplitude_tau = -0.5  # -0.5 corresponds to the regular Hilbert transform
    amplitude_tau = +0.75  # In [-2, 2] it's enough

    # 1. Testing several simple-fdHT. TODO add interesting examples: how?
    if not animation:
        # all_tau = [amplitude_tau, 5 * amplitude_tau, 10 * amplitude_tau, 15 * amplitude_tau]
        all_tau = [amplitude_tau]
        # all_theta = 2 * np.pi * np.random.random(2)  # Random in [0, 2pi] XXX Change number here
        all_theta = [- np.pi / 2, - np.pi / 3, 0, np.pi / 3, np.pi / 2]
        print("\n- Starting tests for one-fdHT ...")
        for tau in all_tau:
            for theta in all_theta:
                print("\nTest n°={} with tau = {} and theta = {}...".format(offset, tau, theta))
                apply_and_plot(tau=tau, theta=theta, offset=offset, lena=lena,
                               # TODO Comment/uncomment the next lines to control which plot is included:
                               plot_fdht=False,
                               # plot_fourier=True,
                               # plot_fdht_fourier=True,
                               # plot_fourier_color=True,
                               plot_color=True,
                               save=save,
                               **kwargs
                              )
                offset += 1
        print("\n---> OK for the tests for one-fdHT.")

    # 2. Testing the multi-fdHT (OK it works as the simple-fdHT)
    if multi:
        all_theta = [[0, np.pi / 4, np.pi / 2, 3 * np.pi / 4]]
        nb = len(all_theta[0])
        all_tau = [[amplitude_tau] * nb]
        print("\n- Starting tests for multi-fdHT ...")
        for tau in all_tau:
            for theta in all_theta:
                print("\nTest n°={} with tau = {} and theta = {}...".format(offset, tau, theta))
                apply_and_plot(tau=tau, theta=theta, offset=offset, lena=lena, save=save, **kwargs)
                offset += 1
        print("\n---> OK for the tests for multi-fdHT.")

    # 3. Test a turning animation of the simple-fdHT
    if animation:
        print("\n- Starting turning animation of the simple-fdHT ...")
        results = apply_and_turn(tau0=amplitude_tau, lena=lena, save=save, turn_input=turn_input, move_tau=move_tau, **kwargs)
        print("\n---> OK for the turning animation of the simple-fdHT.")
        return results


def savegifs(offset=0):
    """ Generating and saving a lot of animations as gif files..."""
    print("Generating and saving a lot of animations as gif files...")
    print("\n\nStarting 'main(lena=False, animation=True, save=True, offset={})' ...".format(offset))
    offset += 1
    main(lena=False, animation=True, save=True, offset=offset)
    offset += 1
    print("\n\nStarting 'main(lena=False, animation=True, save=True, turn_input=True, offset={})' ...".format(offset))
    offset += 1
    main(lena=False, animation=True, save=True, turn_input=True, offset=offset)
    offset += 1
    print("\n\nStarting 'main(lena=False, animation=True, save=True, move_tau=True, offset={})' ...".format(offset))
    offset += 1
    main(lena=False, animation=True, save=True, move_tau=True, offset=offset)
    offset += 1
    print("\n\nStarting 'main(lena=True, animation=True, save=True, offset={})' ...".format(offset))
    offset += 1
    main(lena=True, animation=True, save=True, offset=offset)
    offset += 1
    print("\n\nStarting 'main(lena=True, animation=True, save=True, move_tau=True, offset={})' ...".format(offset))
    offset += 1
    main(lena=True, animation=True, save=True, move_tau=True, offset=offset)


if __name__ == '__main__':
    plt.close('all')
    plt.interactive(True)  # FIXED? It seemed to be blocking, but it's alright ?
    visualizeHSVColorMap()
    offset = 0
    # TODO Comment/uncomment here to change the different settings of the simulation
    offset += 1
    main(lena=True, animation=True, save=False, offset=offset, move_tau=True)
    # for theta0 in [-np.pi, -3 * np.pi / 4, -np.pi / 2, -np.pi / 4, 0, np.pi / 4, np.pi / 2, 3 * np.pi / 4, np.pi]:
    #     offset += 1
    #     print("\n\nStarting 'main(lena=False, animation=True, save=True, move_tau=True, offset={}, theta0={})' ...".format(offset, theta0))
    #     main(lena=True, animation=True, save=True, move_tau=True, offset=offset, theta0=theta0)
    # # origin, turn_input, move_tau, tau0, theta0, all_theta, all_tau, image, imshow1_manager, imshow2_manager, suptitle_manager = main(lena=False, animation=True, save=False)
    # savegifs()  # Generate a lot of gifs...
    print("\n\nTODO: keep working A LITTLE BIT on this script (but not too much) !")  # TODO: keep working A LITTLE BIT on this script (but not too much) !

# End of fdHT.py
