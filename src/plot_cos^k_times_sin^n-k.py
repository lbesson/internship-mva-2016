#! /usr/bin/env python3
# -*- coding: utf-8; mode: python -*-
""" Simple Python 2/3 script to plot some cos^k(theta) sin^{n+1-k}(theta) functions, for all k = 1 ... n.

- *Date:* Monday 18 July 2016.
- *Author:* Lilian Besson, for the MVA Master, (C) 2016.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

from __future__ import absolute_import, print_function, division  # Python 2 compatibility if needed

from sys import argv
from os.path import exists

import numpy as np
import matplotlib.pyplot as plt


def main(n=2, save=True, force=False):
    """ Plot some cos^k(theta) sin^{n+1-k}(theta) functions, for all k = 1 ... n .
    """
    # 1. Computing
    print("\n\nPlotting cos^k(theta) sin^{n+1-k}(theta) functions, for all k = 1 ... n with n =", n, "...")
    N = 3000
    x = np.linspace(0, 2 * np.pi, N)
    y = np.zeros((n, N))
    for i in range(1, n + 1):
        print(r"  Computing y_i = \cos(x)^{} \sin(x)^{}$ ...".format(n + 1 - i, i))
        y[i - 1] = np.cos(x)**(n + 1 - i) * np.sin(x)**(i)
    m = np.max(y)
    y /= m  # We renormalize?
    # 2. Priting
    print("New figure ...")
    plt.figure()
    plt.hold(True)
    plt.grid()
    for i in range(1, n + 1):
        print(r"  Plotting y_i = \cos(x)^{} \sin(x)^{}$ ...".format(n + 1 - i, i))
        plt.plot(x, y[i - 1], label=r'$\cos(x)^{} \sin(x)^{}$'.format(n + 1 - i, i))
    plt.legend()
    plt.xlim([-0.2, 2 * np.pi + 0.2])
    plt.xlabel(r'$x = 0 ... 2 \pi$')
    plt.xticks(
        [0, np.pi / 2, np.pi, 3 * np.pi / 2, 2 * np.pi],
        ['$0$', r'$\pi / 2$', r'$\pi$', r'$3 \pi / 2$', r'$2 \pi$']
    )
    plt.ylim([-1.05, 1.05])
    plt.ylabel(r'$y = -1 ... 1$')
    plt.yticks([-1, -0.5, 0, 0.5, 1])
    plt.title(r'The normalized $x \mapsto \cos^i(x) \sin^{%d-i}(x)$ functions, for all $i = 1 ... %d$' % (n + 1, n))
    print("Done for the figure ... Saving it ...")
    # 3. Saving
    if save:
        outname = "{}.png".format('_and_'.join(['cos{}sin{}'.format(n + 1 - i, i) for i in range(1, n + 1)]))
        # outname = outname.replace('1', '')  # Strips out the useless 1
        if force or not exists(outname):
            print("... Saving to '{}'...".format(outname))
            plt.savefig(outname, dpi=240)
        else:
            print("ERROR: the file '{}' is already present. Pass the option force=True to manually force the erasure of already existing files...".format(outname))
    print("Showing the figure ...")
    plt.show()


if __name__ == '__main__':
    print("Calling the script ...")
    try:
        maxN = int(argv[1])
    except Exception as e:  # DEBUG
        print("ERROR: unable to read the number of plots from the command line (only argument), so using default = 4. argv =", argv)
        maxN = 4
    for n in range(2, maxN + 1):
        main(n, force=True)

# End of plot_cos^k_times_sin^n-k.py
