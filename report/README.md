## Rapport de stage / internship report
My report is completely done, and its LaTeX source code and the PDF are shared here.

See here for the PDF report : https://goo.gl/xPzw4A (latest version)

----

### [Some LaTeX-related statistics](latexstats.txt)

### *Question:* How to compile this document ?
It's easy:

1. Clone the repository: `git clone https://bitbucket.org/lbesson/internship-mva-2016`
2. Go into the folder `report`
3. (Optional) Create a symlink for `../fig` in `report`: `ln -s ../fig ./`
4. And simply run `make pdf clean`
5. (Optional) You can compress the PDF with my script [PDFCompress](https://bitbucket.org/lbesson/bin/src/master/PDFCompress) if you want

> - Tested only on Linux (XUbuntu 15.04), but it should also work on Mac OS X (with GNU `make`, `pdflatex` and `bibtex`) and possibly on Windows.
> - Step 1 is needed to have [the BibTeX file](../biblio/) and step 3 is needed to have [the figures](../fig/).

### *Question:* can I use the source of this document or [this part of this document] ?
Yes, of course, as long as you respect the terms of [the MIT License](../LICENSE) and **you cite my work** in your report / article / whatever.

> Be fair, be smart, please.

----

#### More ?
> See [the slides](../slides/), [the code](../src/) or [the figures](../fig/) if needed.

> [Main directory?](../) | [License?](../LICENSE) | [Some git statistics?](../complete-stats.txt)
> © 2016 [Lilian Besson](http://perso.crans.org/besson/)
