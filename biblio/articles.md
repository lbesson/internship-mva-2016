## List of reference articles
> TODO : make a nice BibTeX file instead

----

- "Le modèle d'innovation en traitement du signal", internship report by Julien Fageot, 2011
- "On the Shiftability of Dual-Tree Complex Wavelet Transforms", by Kunal Narayan Chaudhury and Michael Unser, 2013
- "Statistics of wavelet coefficients for sparse self-similar images", by Julien Fageot, Emrah Bostan, and Michael Unser, 2014
- "Self-Similarity: Part I — Splines and Operators", by Michael Unser and Thierry Blu, 2007
- NOPE "Self-similarity: Part II — Optimal estimation of fractal processes", by Michael Unser and Thierry Blu, 2007

- "Spline solutions to L1 Extremal Problems in One and Several Variables", by S. D. Fisher and J. W. Jerome, 1975
- "Fractional Splines and Wavelets", by Michael Unser and Thierry Blu, 2000
- "Splines are universal solutions of linear inverse problems with generalized-TV regularization", by Michael Unser, Julien Fageot, and John Paul Ward, 2015
- "A Unifying Parametric Framework for 2D Steerable Wavelet Transforms", by Michael Unser, Nicolas Chenouard, 2013
- "Decay Properties of Riesz Transforms and Steerable Wavelets", by John Paul Ward, Kunal Narayan Chaudhury, and Michael Unser, 2013
- "On the Continuity of Characteristic Functionals and Sparse Stochastic Modeling", by Julien Fageot, Arash Amini and Michael Unser, 2014

- "Fractional Hilbert transform extensions and associated analytic signal construction", by Arun Venkitaraman, Chandra Sekhar Seelamantula, 2013
- "The Fractional Hilbert Transform and Dual-Tree Gabor-Like Wavelet Analysis", by Kunal Narayan Chaudhury, and Michael Unser, 2009

- "Steerable PCA for Rotation-Invariant Image Recognition", by Cédric Vonesch, Frédéric Stauber, and Michael Unser, 2015.
- "Canonical decomposition of steerable functions", by Yacov Hel-Or and Patrick C. Teo, 1998.


# Numerical aspects
- "Probing the Pareto Frontier for Basis Pursuit Solutions", by Ewout van den Berg and Michael p. Friedlander, 2008
- "l1-magic :  Recovery of Sparse Signals via Convex Programming", by Emmanuel Candès and Justin Romberg, 2005
- ?
