# Quick Makefile to easily compile the slides (make pdf) and run the slide (make showtime)
default: showtime

all: clean nocolorpdf clean sign

# This is NOT working...
total:
	time ( make -B clean pdf ; make -B clean evince ) || alert

screens:
	arandr

showtime:	slides
slides:
	#-gmusicbrowser -cmd Pause  # Just to make sure your music player is NOT playing when you start your talk
	@echo "Using Impressive.py (from http://impressive.sourceforge.net/)"
	impressive.py --fullscreen --page-progress --mousedelay 500 --tracking --transtime 0 --duration 35m MVA__Internship__Lilian_Besson__2016__Slides.en.pdf | tee timestats`date +"__%d-%m__%H:%M"`.txt
	# http://impressive.sourceforge.net/manual.php

# Construction rules
pdf:
	#pdflatex MVA__Internship__Lilian_Besson__2016__Slides.en.tex
	pdflatex MVA__Internship__Lilian_Besson__2016__Slides.en.tex
	# We have to include BibTeX
	+bibtex  MVA__Internship__Lilian_Besson__2016__Slides.en
	pdflatex MVA__Internship__Lilian_Besson__2016__Slides.en.tex
	pdflatex MVA__Internship__Lilian_Besson__2016__Slides.en.tex
	-pdfinfo MVA__Internship__Lilian_Besson__2016__Slides.en.pdf | grep -v ':[ \t]\+no' | grep --color=always "^[A-Za-z ]\+:"

nocolorpdf:
	#/usr/bin/pdflatex -synctex=1 -file-line-error MVA__Internship__Lilian_Besson__2016__Slides.en.tex
	/usr/bin/pdflatex -synctex=1 -file-line-error MVA__Internship__Lilian_Besson__2016__Slides.en.tex
	# We have to include BibTeX
	+bibtex  MVA__Internship__Lilian_Besson__2016__Slides.en
	/usr/bin/pdflatex -synctex=1 -file-line-error MVA__Internship__Lilian_Besson__2016__Slides.en.tex
	/usr/bin/pdflatex -synctex=1 -file-line-error MVA__Internship__Lilian_Besson__2016__Slides.en.tex
	-pdfinfo MVA__Internship__Lilian_Besson__2016__Slides.en.pdf | grep -v ':[ \t]\+no' | grep --color=always "^[A-Za-z ]\+:"

sign:
	gpg --armor --detach-sign --yes --no-batch --use-agent MVA__Internship__Lilian_Besson__2016__Slides.en.pdf

bst:
	tex naereen.dbj
	mv -vf naereen.log /tmp/

# Experiment. Useless !
handout:
	cat MVA__Internship__Lilian_Besson__2016__Slides.en.tex | sed s/']{beamer}'/',handout]{beamer}'/ > MVA__Internship__Lilian_Besson__2016__Slides_handout.en.tex
	-diff MVA__Internship__Lilian_Besson__2016__Slides.en.tex MVA__Internship__Lilian_Besson__2016__Slides_handout.en.tex
	pdflatex MVA__Internship__Lilian_Besson__2016__Slides_handout.en.tex
	+bibtex MVA__Internship__Lilian_Besson__2016__Slides_handout.en
	pdflatex MVA__Internship__Lilian_Besson__2016__Slides_handout.en.tex
	pdflatex MVA__Internship__Lilian_Besson__2016__Slides_handout.en.tex
	mv -vf MVA__Internship__Lilian_Besson__2016__Slides_handout.en.tex /tmp/
	-pdfinfo MVA__Internship__Lilian_Besson__2016__Slides_handout.en.pdf | grep -v ':[ \t]\+no' | grep --color=always "^[A-Za-z ]\+:"


# Utility for the PDF
evince:
	-evince MVA__Internship__Lilian_Besson__2016__Slides.en.pdf &

compress:
	PDFCompress MVA__Internship__Lilian_Besson__2016__Slides.en.pdf

# Cleaner
c:	clean

clean:
	-mv -vf *.fls *.fdb_latexmk *.ps *.dvi *.htoc *.tms *.tid *.lg *.log *.id[vx] *.vrb *.toc *.snm *.nav *.htmp *.aux *.tmp *.out *.haux *.hidx *.bbl *.blg *.brf *.lof *.ilg *.ind *.meta *.fdb_latexmk *.fls *.gz*busy* /tmp/ 2>/dev/null

latexstats:
	latexstats.sh MVA__Internship__Lilian_Besson__2016__Slides.en.tex
	latexstats.sh MVA__Internship__Lilian_Besson__2016__Slides.en.tex | sed -r "s:\x1B\[[0-9;]*[mK]::g" > latexstats.txt

# FIXME try and update the commands if needed?
2screenON:
	xrandr --output LVDS1 --mode 1024x768 --primary
	# force use 1024x768 mode of the projector
	xrandr --output VGA1 --mode 1024x768 --right-of LVDS1 || (xrandr --addmode VGA1 1024x768 && xrandr --output VGA1 --mode 1024x768 --right-of LVDS1)

2screenOFF:
	xrandr --output VGA1 --off
	xrandr --output LVDS1 --auto

# Sender
upload:
	CP MVA__Internship__Lilian_Besson__2016__Slides.en.pdf besson@zamok.crans.org:~/www/publis/mva-2016/MVA_2015-16__Internship_at_EPFL__Lilian_Besson__Slides.en.pdf

# Linters!
lint:	write-good.sh write-good

proselint:
	time proselint MVA__Internship__Lilian_Besson__2016__Slides.en.tex | tee proselint_report.txt

write-good:
	time write-good --no-passive --no-weasel --no-tooWordy --no-adverb MVA__Internship__Lilian_Besson__2016__Slides.en.tex | tee writegood_report.txt

write-good.sh:
	time write-good.sh MVA__Internship__Lilian_Besson__2016__Slides.en.tex | tee writegood_sh_report.txt
