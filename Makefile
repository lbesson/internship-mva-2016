#!/usr/bin/env make
# Author:	Lilian BESSON
# Email:	lilian DOT besson AT ens-cachan D O T org
# Version:	2
# Date:		11-08-16
#
# Makefile for the git repository internship-mva-2016
# https://bitbucket.org/lbesson/internship-mva-2016
#
###############################################################################
# Custom items
# CP = /usr/bin/rsync --verbose --compress --human-readable --progress --archive --delete --exclude=\.htaccess
# CP = scp
CP = /home/lilian/bin/CP --delete --exclude=\.htaccess --exclude=\.git --exclude=.*~
GPG = gpg --no-batch --use-agent --detach-sign --armor --quiet --yes

git:
	git add README.md Makefile
	git commit -m "Auto commit with 'make git'."
	git push

all:   sendAll

# bib.html, clean, stats etc

bibhtml:
	cd biblio ; bibtex2html -s ../report/naereen.bst -u -charset utf-8 -linebreak *.bib ; cd ..

clean:
	cd report ; make clean ; cd ..
	cd slides ; make clean ; cd ..

stats:
	-echo "\nCommit stats :" | tee ./complete-stats.txt
	+git-complete-stats.sh   | tee -a ./complete-stats.txt
	-echo "\nCalendar :"     | tee -a ./complete-stats.txt
	-git-cal --ascii         | tee -a ./complete-stats.txt
	git wdiff ./complete-stats.txt

cloudwords:
	-generate-word-cloud.py -s -m 180 -t "Words from LaTeX sources - MVA Internship - (C) 2016 Lilian Besson" ./*.md ./*.txt */*.md slides/*.tex report/*.tex
	generate-word-cloud.py -f -o cloudwords_latex_sources.png -m 180 -t "Words from LaTeX sources - MVA Internship - (C) 2016 Lilian Besson" ./*.md ./*.txt */*.md slides/*.tex report/*.tex
	-generate-word-cloud.py -s -m 180 -t "Words from Python code - MVA Internship - (C) 2016 Lilian Besson" */*.py
	generate-word-cloud.py -f -o cloudwords_python_code.png -m 180 -t "Words from Python code - MVA Internship - (C) 2016 Lilian Besson" */*.py


# Archive

notify_archive: archive
	notify-send "MVA : archiving" "Generating archive : done ! (~/Dropbox/Internship-MVA-2016.zip)"

archive:
	zip -y -r -9 /home/lilian/Internship-MVA-2016.zip /home/lilian/stageM2/[a-zA-Z]* /home/lilian/stageM2/.gitignore /home/lilian/stageM2/.htaccess
	$(GPG) /home/lilian/Internship-MVA-2016.zip
	mv -vf /home/lilian/*MVA*.zip* /home/lilian/Dropbox/

fixperms:
	# @echo "Fixing (badly) a recurrent bug..."
	# +cd /home/lilian/stageM2/
	@echo "Fixing permissions (chmod o-w, g-w) before uploading..."
	#-rm -fv *~ .*~ .*/*~ .*/*/.*~ .*/*/*~
	-chmod -vR o-w /home/lilian/stageM2/ | tee /tmp/MVAperms_o.log  | grep modifi | grep -v symbolique
	-chmod -vR g-w /home/lilian/stageM2/ | tee /tmp/MVAperms_g.log  | grep modifi | grep -v symbolique

# Senders

sendAll: notify_archive send_zamok

send: send_zamok

send_zamok: fixperms
	# cd /home/lilian/stageM2/
	$(CP) -r /home/lilian/stageM2/[a-zA-Z]* /home/lilian/stageM2/.gitignore /home/lilian/stageM2/.htaccess besson@zamok.crans.org:~/www/stageM2/
	#-$(CP) ~/Dropbox/mva.* besson@zamok.crans.org:~/www/dl/
